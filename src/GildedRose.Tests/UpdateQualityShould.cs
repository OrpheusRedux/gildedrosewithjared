﻿using Xunit;
using GildedRose.Console;
using System.Collections.Generic;

namespace GildedRose.Tests
{
    public class UpdateQualityShould
    {
        [Fact]
        public static void DecrementQualityByOne_GivenNormalItem()
        {
            ItemProxy item = GetNormalItem();
            int initialQuality = item.Quality;
            Program app = new Program(new List<ItemProxy> { item });

            app.UpdateQuality();

            Assert.Equal(initialQuality - 1, item.Quality);
        }

        private static ItemProxy GetNormalItem()
        {
            return new ItemProxy { Name = "Normal Item", Quality = 10, SellIn = 10 };
        }

        [Fact]
        public static void DecrementSellInByOne()
        {
            ItemProxy item = GetNormalItem();
            int initialSellIn = item.SellIn;
            Program app = new Program(new List<ItemProxy> { item });

            app.UpdateQuality();

            Assert.Equal(initialSellIn - 1, item.SellIn);
        }

        [Theory]
        [InlineData(0)]
        [InlineData(-1)]
        [InlineData(-2)]
        public static void DecrementQualityByTwo_GivenExpiredNormalItem(int testSellIn)
        {
            ItemProxy item = GetNormalItem();
            int initialQuality = item.Quality;
            item.SellIn = testSellIn;
            Program app = new Program(new List<ItemProxy> { item });

            app.UpdateQuality();

            Assert.Equal(initialQuality - 2, item.Quality);
        }

        [Fact]
        public static void ClampQualityAtZero_GivenNormalItem()
        {
            ItemProxy item = GetNormalItem();
            item.Quality = 0;
            Program app = new Program(new List<ItemProxy> { item });

            app.UpdateQuality();

            Assert.False(item.Quality < 0);
        }

        [Fact]
        public static void IncrementQualityByOne_GivenAgedBrie()
        {
            ItemProxy brie = GetAgedBrie();
            int initialQuality = brie.Quality;
            Program app = new Program(new List<ItemProxy> { brie });

            app.UpdateQuality();

            Assert.Equal(initialQuality + 1, brie.Quality);
        }

        private static ItemProxy GetAgedBrie()
        {
            return new ItemProxy { Name = "Aged Brie", Quality = 10, SellIn = 10 };
        }

        [Theory]
        [InlineData(0)]
        [InlineData(-1)]
        [InlineData(-2)]
        public static void IncrementQualityByTwo_GivenExpiredAgedBrie(int testSellIn)
        {
            ItemProxy brie = GetAgedBrie();
            int initialQuality = brie.Quality;
            brie.SellIn = testSellIn;
            Program app = new Program(new List<ItemProxy> { brie });

            app.UpdateQuality();

            Assert.Equal(initialQuality + 2, brie.Quality);
        }

        [Fact]
        
        public static void ClampQualityAtFiftyGivenAgedBrie()
        {
            ItemProxy brie = GetAgedBrie();
            brie.Quality = 50;
            Program app = new Program(new List<ItemProxy> { brie });

            app.UpdateQuality();

            Assert.False(brie.Quality > 50);
        }

        [Fact]

        public static void NotAlterQualityGivenSulfuras()
        {
            ItemProxy sulfuras = GetSulfuras();
            int initialQuality = sulfuras.Quality;
            Program app = new Program(new List<ItemProxy> { sulfuras });

            app.UpdateQuality();

            Assert.Equal(initialQuality, sulfuras.Quality);
        }

        private static ItemProxy GetSulfuras()
        {
            return new ItemProxy { Name = "Sulfuras, Hand of Ragnaros", Quality = 80, SellIn = 0 };
        }

        [Fact]

        public static void NotAlterSellinGivenSulfuras()
        {
            ItemProxy sulfuras = GetSulfuras();
            int initialSellin = sulfuras.SellIn;
            Program app = new Program(new List<ItemProxy> { sulfuras });

            app.UpdateQuality();

            Assert.Equal(initialSellin, sulfuras.SellIn);
        }

        [Fact]

        public static void IncrementQualityByOneGiveBackstagePassSellinGreaterThanTen()
        {
            ItemProxy pass = GetBackstagePass();
            int initialQuality = pass.Quality;
            Program app = new Program(new List<ItemProxy> { pass });

            app.UpdateQuality();

            Assert.Equal(initialQuality + 1, pass.Quality);
        }

        private static ItemProxy GetBackstagePass()
        {
            return new ItemProxy { Name = "Backstage passes to a TAFKAL80ETC concert", Quality = 20, SellIn = 15 };
        }

        [Theory]
        [InlineData(10)]
        [InlineData(9)]
        [InlineData(8)]
        [InlineData(7)]
        [InlineData(6)]

        public static void IncrementQualityByTwoGivenBackstagePassSellinGreaterThanFiveAndLessThanOrEqualToTen(int sellIn)
        {
            ItemProxy pass = GetBackstagePass();
            int initialQuality = pass.Quality;
            pass.SellIn = sellIn;
            Program app = new Program(new List<ItemProxy> { pass });

            app.UpdateQuality();

            Assert.Equal(initialQuality + 2, pass.Quality);
        }

        [Theory]
        [InlineData(5)]
        [InlineData(4)]
        [InlineData(3)]
        [InlineData(2)]
        [InlineData(1)]


        public static void IncrementQualityByThreeGivenBackStagePassSellinGreaterThanZeroAndLessThanOrEqualToFive(int sellIn)
        {
            ItemProxy pass = GetBackstagePass();
            int initialQuality = pass.Quality;
            pass.SellIn = sellIn;
            Program app = new Program(new List<ItemProxy> { pass });

            app.UpdateQuality();

            Assert.Equal(initialQuality + 3, pass.Quality);
        }

        [Theory]
        [InlineData(0)]
        [InlineData(-1)]
        [InlineData(-2)]

        public static void SetQualityToZeroGivenBackStagePassSellinExpired(int sellIn)
        {
            ItemProxy pass = GetBackstagePass();
            pass.SellIn = sellIn;
            Program app = new Program(new List<ItemProxy> { pass });

            app.UpdateQuality();

            Assert.Equal(0, pass.Quality);
        }
    }
}
