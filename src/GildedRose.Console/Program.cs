﻿using GildedRose.Console.Rules;
using System;
using System.Collections.Generic;

namespace GildedRose.Console
{
    public class Program
    {
        IList<ItemProxy> Items;
        IList<IRule> rules;

        public Program(List<ItemProxy> items)
        {
            Items = items;
            PopulateRulesList();
        }

        private void PopulateRulesList()
        {
            rules = new List<IRule>();
            rules.Add(new Rule_Aged_Brie_Increment_Quality_Unexpired());
            rules.Add(new Rule_Aged_Brie_Increment_Quality_Expired());
            rules.Add(new Rule_Normal_Item_Quality_Unexpired());
            rules.Add(new Rule_Normal_Item_Quality_Expired());
            rules.Add(new Rule_Backstage_Pass_Greater_Than_10_SellIn_Quality());
            rules.Add(new Rule_Backstage_Pass_Greater_Than_5_And_Less_Than_10_SellIn_Quality());
            rules.Add(new Rule_Backstage_Pass_Greater_Than_0_But_Less_Than_5_SellIn_Quality());
            rules.Add(new Rule_Backstage_Pass_Expired());
            rules.Add(new Rule_Decrement_SellIn());
        }

        public Program()
        {
            PopulateRulesList();
        }

        static void Main()
        {
            System.Console.WriteLine("OMGHAI!");

            var app = new Program()
            {
                Items = new List<ItemProxy>
                {
                    new ItemProxy { Name = "+5 Dexterity Vest", SellIn = 10, Quality = 20 },
                    new ItemProxy { Name = "Aged Brie", SellIn = 2, Quality = 0 },
                    new ItemProxy { Name = "Elixir of the Mongoose", SellIn = 5, Quality = 7 },
                    new ItemProxy { Name = "Sulfuras, Hand of Ragnaros", SellIn = 0, Quality = 80 },
                    new ItemProxy { Name = "Backstage passes to a TAFKAL80ETC concert", SellIn = 15, Quality = 20 },
                    new ItemProxy { Name = "Conjured Mana Cake", SellIn = 3, Quality = 6 }
                }
            };

            app.UpdateQuality();

            System.Console.ReadKey();

        }

        public void UpdateQuality()
        {
            foreach (ItemProxy item in Items)
            {
                foreach (IRule rule in rules)
                {
                    if (rule.Applies(item))
                    {
                        rule.Execute(item);
                    }
                }
            }
        }
    }
}
