﻿
namespace GildedRose.Console.Rules
{
    interface IRule
    {
        bool Applies(ItemProxy item);
        void Execute(ItemProxy item);
    }
}
