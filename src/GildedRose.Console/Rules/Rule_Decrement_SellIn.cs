﻿namespace GildedRose.Console.Rules
{
    public class Rule_Decrement_SellIn : IRule
    {
        public bool Applies(ItemProxy item)
        {
            return (item.Name != "Sulfuras, Hand of Ragnaros");
        }

        public void Execute(ItemProxy item)
        {
            item.SellIn -= 1;
        }
    }
}
