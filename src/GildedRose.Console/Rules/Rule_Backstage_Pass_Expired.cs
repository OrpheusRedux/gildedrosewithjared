﻿namespace GildedRose.Console.Rules
{
    class Rule_Backstage_Pass_Expired : IRule
    {
        public bool Applies(ItemProxy item)
        {
            return (item.Name == "Backstage passes to a TAFKAL80ETC concert" && item.SellIn <= 0);
        }

        public void Execute(ItemProxy item)
        {
            item.Quality = 0;
        }
    }
}
