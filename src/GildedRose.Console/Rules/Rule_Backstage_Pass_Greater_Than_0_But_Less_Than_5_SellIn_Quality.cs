﻿namespace GildedRose.Console.Rules
{
    class Rule_Backstage_Pass_Greater_Than_0_But_Less_Than_5_SellIn_Quality : IRule
    {
        public bool Applies(ItemProxy item)
        {
            return (item.Name == "Backstage passes to a TAFKAL80ETC concert" && item.SellIn > 0 && item.SellIn <= 5);
        }

        public void Execute(ItemProxy item)
        {
            item.Quality += 3;
        }
    }
}
