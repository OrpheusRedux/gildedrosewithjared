﻿namespace GildedRose.Console.Rules
{
    public class Rule_Normal_Item_Quality_Expired : IRule
    {
        public bool Applies(ItemProxy item)
        {
            return (item.Name != "Sulfuras, Hand of Ragnaros"
                && item.Name != "Aged Brie"
                && item.Name != "Backstage passes to a TAFKAL80ETC concert"
                && item.SellIn <= 0);
        }
        public void Execute(ItemProxy item)
        {
            item.Quality -= 2;
        }
    }
}
