﻿namespace GildedRose.Console.Rules
{
    public class Rule_Aged_Brie_Increment_Quality_Expired : IRule
    {
        public bool Applies(ItemProxy item)
        {
            return (item.Name == "Aged Brie" && item.SellIn <= 0);
        }
        public void Execute(ItemProxy item)
        {
            item.Quality += 2;
        }
    }
}
