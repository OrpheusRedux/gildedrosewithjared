﻿namespace GildedRose.Console.Rules
{
    class Rule_Backstage_Pass_Greater_Than_5_And_Less_Than_10_SellIn_Quality : IRule
    {
        public bool Applies(ItemProxy item)
        {
            return (item.Name == "Backstage passes to a TAFKAL80ETC concert" && item.SellIn > 5 && item.SellIn <= 10);
        }

        public void Execute(ItemProxy item)
        {
            item.Quality += 2;
        }
    }
}
