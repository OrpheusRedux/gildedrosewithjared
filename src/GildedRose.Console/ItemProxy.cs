﻿

namespace GildedRose.Console
{
    public class ItemProxy
    {
        private readonly Item item = new Item();

        public string Name
        {
            get => item.Name;
            set => item.Name = value;
        }

        public int SellIn
        {
            get => item.SellIn;
            set => item.SellIn = value;
        }

        public int Quality
        {
            get => item.Quality;
            set
            {
                if (value > 50)
                {
                    item.Quality = 50;
                }

                else if (value < 0)
                {
                    item.Quality = 0;
                }

                else
                {
                    item.Quality = value;
                }
            }
        }
    }
}
